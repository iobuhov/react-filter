import { ReduceStore } from 'flux/utils';
import { Map, Set } from 'immutable';
import FilterStoreRecord from 'records/FilterStoreRecord';
import FilterDispatcher from 'dispatchers/FilterDispatcher';
import FilterConstants from '../constants/FilterConstants';

const log = (...args) => //eslint-disable-line
      console.log.apply(console, ['[STORE]', ...args]);

const storeInit = action =>
  new FilterStoreRecord({ ...action.spec });

const addContext = action => tree =>
      tree.set(action.id, Map());

const removeContext = action => tree =>
      tree.remove(action.id);

const addDimension = action => tree =>
      tree.setIn(
        [action.tableId, action.id],
        Set()
      );

const removeDimension = action => tree =>
      tree.update(
        action.tableId,
        table => table.remove(action.id)
      );

const pushRecords = action => records =>
      records.concat(action.records);

const updateTree = (state, updater) =>
      state.update('selectedTree', updater);

const toggleContext = ({ state, action }) =>
      updateTree(
        state,
        action.isChecked ?
          addContext(action) :
          removeContext(action)
      );

const toggleDimension = ({ state, action }) =>
      updateTree(
        state,
        action.isChecked ?
          addDimension(action) :
          removeDimension(action)
      );

const toggleRecord = ({ state, action }) =>
      updateTree(
        state,
        tree => tree.updateIn(
          [action.tableId, action.columnId],
          records => records[action.isChecked ? 'add' : 'remove'](action.id)
        )
      );

const maybePushRecords = ({ state, action }) => {
  const isNewRecords = !state.records.isSuperset(
    action.records.values()
  );
  if (isNewRecords) {
    return state.update('records', pushRecords(action));
  }
  return state;
};

class FilterStore extends ReduceStore {
  constructor() {
    super(FilterDispatcher);
  }

  getInitialState() {
    return new FilterStoreRecord();
  }

  reduce(state, action) {
    switch (action.type) {
      case FilterConstants.GET_INIT_DATA_SUCCESS:
        return storeInit(action);
      case FilterConstants.GET_RECORDS_SUCCESS:
        return maybePushRecords({ state, action });
      case FilterConstants.TOGGLE_CONTEXT:
        return toggleContext({ state, action });
      case FilterConstants.TOGGLE_DIMENSION:
        return toggleDimension({ state, action });
      case FilterConstants.TOGGLE_RECORD:
        return toggleRecord({ state, action });
      default:
        return state;
    }
  }
}

export default new FilterStore();
