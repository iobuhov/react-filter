import { ReduceStore } from 'flux/utils';
import { Map } from 'immutable';
import FilterDispatcher from 'dispatchers/FilterDispatcher';
import SearchConstants from 'constants/SearchConstants';
import SearchStoreRecord from 'records/SearchStoreRecord';
import SearchTypeRecord from 'records/SearchTypeRecord';

const startWith = (source, search) =>
      source.startsWith(search);

const includes = (source, search) =>
      source.includes(search);

const exactMatch = (source, search) =>
      source === search;

class SearchStore extends ReduceStore {
  constructor() {
    super(FilterDispatcher);
  }

  getInitialState() {
    return new SearchStoreRecord({
      types: Map([
        [1, new SearchTypeRecord({ id: 1, predicate: startWith, label: '^*' })],
        [2, new SearchTypeRecord({ id: 2, predicate: includes, label: '*_' })],
        [3, new SearchTypeRecord({ id: 3, predicate: exactMatch, label: '**' })],
      ]),
      predicate: startWith,
    });
  }

  reduce(state, action) {
    switch (action.type) {
      case SearchConstants.SEARCH_STRING_CHANGE:
        return state.set('searchString', action.str);

      case SearchConstants.SEARCH_TYPE_CHANGE:
        return state.set(
          'predicate',
          state.types.getIn([action.id, 'predicate'])
        );

      case SearchConstants.SEARCH_SORT_CHANGE:
        return state.update('sortAlphabetically', flag => !flag);

      default:
        return state;
    }
  }
}

export default new SearchStore();
