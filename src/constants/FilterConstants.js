const FilterConstants = {
  GET_INIT_DATA_SUCCESS: 'GET_INIT_DATA_SUCCESS',
  GET_RECORDS: 'GET_RECORDS',
  GET_RECORDS_SUCCESS: 'GET_RECORDS_SUCCESS',
  INIT: 'INIT',
  REQUEST_FAIL: 'REQUEST_FAIL',
  TOGGLE_CONTEXT: 'TOGGLE_CONTEXT',
  TOGGLE_DIMENSION: 'TOGGLE_DIMENSION',
  TOGGLE_RECORD: 'TOGGLE_RECORD',
};

export default FilterConstants;
