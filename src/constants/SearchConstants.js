export default {
  SEARCH_STRING_CHANGE: 'SEARCH_STRING_CHANGE',
  SEARCH_TYPE_CHANGE: 'SEARCH_TEXT_CHANGE',
  SEARCH_SORT_CHANGE: 'SEARCH_SORT_CHANGE',
};
