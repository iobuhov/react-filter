import { render as ReactDOMRender } from 'react-dom';
import { AppContainer } from 'react-hot-loader';
import React from 'react';
import App from 'components/App/App';
import './index.css';

const ROOT = document.getElementById('app-root');
const render = (Component) => {
  ReactDOMRender(
    <AppContainer>
      <Component />
    </AppContainer>,
    ROOT
  );
};

render(App);

if (module.hot) {
  module.hot.accept('components/App/App', () => {
    const next = require('components/App/App').default;
    render(next);
  }
  );
}
