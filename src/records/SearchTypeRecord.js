import { Record } from 'immutable';

export default new Record({
  id: null,
  predicate: null,
  label: '',
});
