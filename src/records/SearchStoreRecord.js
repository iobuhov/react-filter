import { Record, Map } from 'immutable';

export default new Record({
  types: Map(),
  predicate: null,
  searchString: '',
  sortAlphabetically: false,
});
