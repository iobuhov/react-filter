import { Record } from 'immutable';

export default new Record({
  id: null,
  name: '',
  tableId: null,
});
