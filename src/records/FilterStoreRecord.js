import { Record, List, Map } from 'immutable';

const FilterStoreRecord = new Record({
  rawData: List(),
  contexts: List(),
  dimensions: List(),
  records: Map(),
  selectedTree: Map(),
});

export default FilterStoreRecord;
