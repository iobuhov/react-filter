import { Record } from 'immutable';

export default new Record({
  columnId: null,
  id: '',
  name: '',
  tableId: null,
});
