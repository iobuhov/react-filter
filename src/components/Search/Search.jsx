import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import {
  searchTypeChange,
  searchStringChange,
  searchSortChange,
} from 'actions/SearchActions';
import SearchStore from 'stores/SearchStore';
import Select from 'components/Select/Select';
import './Search.css';

const clickExtractor = item => searchTypeChange(item.id);

const idExtractor = item => item.id;

const keyExtractor = item => item.hashCode();

const labelExtractor = item => item.label;

const onSearchStringChange = str => searchStringChange(str);

class Search extends PureComponent {
  static propTypes = {
    className: PropTypes.string,
  };

  static defaultProps = {
    className: '',
  };

  state = { searchState: SearchStore.getState() };

  componentDidMount() {
    this.token = SearchStore.addListener(this.onStoreChange);
  }

  componentWillUnmount() {
    this.token.remove();
  }

  onStoreChange = () =>
    this.setState({ searchState: SearchStore.getState() })

  getSelected = () =>
    this.state.searchState.types.find(
      type => type.predicate === this.state.searchState.predicate
    )

  render() {
    const rootClass = classNames('search-bar', this.props.className);
    const sortBtnClass = classNames(
      'search-bar__sort-btn',
      { 'search-bar__sort-btn--active': this.state.searchState.sortAlphabetically }
    );
    return (
      <div className={rootClass}>
        <input
          id='search-input'
          className='search-bar__input'
          onChange={e => onSearchStringChange(e.currentTarget.value)}
          value={this.state.searchState.searchString}
          type='text'
        />
        <button className={sortBtnClass} onClick={searchSortChange}>
          A-Z
        </button>
        <Select
          className='search-bar__select'
          items={this.state.searchState.types.toList()}
          selectedId={this.getSelected().id}
          clickExtractor={clickExtractor}
          idExtractor={idExtractor}
          keyExtractor={keyExtractor}
          labelExtractor={labelExtractor}
        />
      </div>
    );
  }
}

export default Search;
