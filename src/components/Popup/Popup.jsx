import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

export default class Popup extends PureComponent {
  static propTypes = {
    className: PropTypes.string,
    onClose: PropTypes.func.isRequired,
    children: PropTypes.oneOfType([
      PropTypes.arrayOf(PropTypes.node),
      PropTypes.node,
    ]),
  };

  static defaultProps = {
    className: 'Popup',
    children: null,
  };

  componentDidMount() {
    document.addEventListener('click', this.handleClick, false);
  }

  componentWillUnmount() {
    document.removeEventListener('click', this.handleClick, false);
  }

  refCallback = (node) => {
    this.node = node;
  }

  handleClick = (event) => {
    const isOuterEvent = !this.node.contains(event.target);
    if (isOuterEvent) {
      this.props.onClose(event);
    }
  }

  render() {
    return (
      <div className={this.props.className} ref={this.refCallback}>
        {this.props.children}
      </div>
    );
  }
}
