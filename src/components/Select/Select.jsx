import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import classNames from 'classnames';
import Popup from 'components/Popup/Popup';
import './Select.css';

export default class Select extends PureComponent {
  static propTypes = {
    className: PropTypes.string,
    clickExtractor: PropTypes.func.isRequired,
    selectedId: PropTypes.oneOfType([
      PropTypes.number,
      PropTypes.string,
    ]),
    items: ImmutablePropTypes.iterable.isRequired,
    idExtractor: PropTypes.func.isRequired,
    keyExtractor: PropTypes.func.isRequired,
    labelExtractor: PropTypes.func.isRequired,
  };

  static defaultProps = {
    selectedId: null,
    className: '',
  };

  state = { isOpen: false };

  toggle = () => this.setState({ isOpen: !this.state.isOpen });

  isSelected = item =>
    this.props.idExtractor(item) === this.props.selectedId;

  render() {
    const {
      className,
      clickExtractor,
      items,
      keyExtractor,
      labelExtractor,
    } = this.props;
    const selected = items.filter(this.isSelected);
    const rest = items.filterNot(this.isSelected);
    const rootClass = classNames('select', className);
    const elements = rest.map(item => (
      <div
        key={keyExtractor(item)}
        onClick={() => clickExtractor(item)}
        className='select__option'
      >
        {labelExtractor(item)}
      </div>
    ));
    return (
      <div className={rootClass} onClick={this.toggle}>
        <div className='select__label'>
          {selected.map(labelExtractor)}
        </div>
        {this.state.isOpen &&
          <Popup className={'select__popup'} onClose={this.toggle}>
            {elements}
          </Popup>
        }
      </div>
    );
  }
}
