import React from 'react';
import FilterContainer from 'containers/FilterContainer';

// leave as it is, because of eslint
export default function App() {
  return (
    <div className='app'>
      <h1 className='app__header'>
        React task
      </h1>
      <FilterContainer />
    </div>
  );
}
