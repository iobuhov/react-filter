import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import CheckBoxList from 'components/CheckBoxList/CheckBoxList';
import Popup from 'components/Popup/Popup';
import classNames from 'classnames';
import './Dropdown.css';

export default class Dropdown extends PureComponent {
  static propTypes = {
    className: PropTypes.string,
    disabled: PropTypes.bool,
    label: PropTypes.string.isRequired,
    selected: ImmutablePropTypes.iterable.isRequired,
    items: ImmutablePropTypes.iterable.isRequired,
    idExtractor: PropTypes.func.isRequired,
    keyExtractor: PropTypes.func.isRequired,
    labelExtractor: PropTypes.func.isRequired,
    handlerExtractor: PropTypes.func.isRequired,
  };

  static defaultProps = {
    className: '',
    disabled: false,
  };

  state = { isOpen: false };

  toggle = () => {
    if (!this.props.disabled) {
      this.setState({ isOpen: !this.state.isOpen });
    }
  }

  render() {
    const {
      label,
      className,
      disabled,
      selected,
      ...props
    } = this.props;
    const rootClass = classNames(
      'dropdown',
      className,
      { 'dropdown--disabled': disabled }
    );
    return (
      <div className={rootClass}>
        <div className='dropdown__inner'>
          <button className='dropdown__btn' onClick={this.toggle}>
            {label}
          </button>
          <span className='dropdown__selected'>
            {selected.map(props.labelExtractor).toArray().join(', ')}
          </span>
        </div>
        {this.state.isOpen && (
          <Popup className={'dropdown__popup'} onClose={this.toggle}>
            <CheckBoxList
              selected={selected.map(props.idExtractor).toSet()}
              {...props}
            />
          </Popup>
        )}
      </div>
    );
  }
}
