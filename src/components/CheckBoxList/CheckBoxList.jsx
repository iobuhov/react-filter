import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import { Set } from 'immutable';

function log(...args) { // eslint-disable-line no-unused-vars
  console.log.apply(console, ['[CHECKBOXLIST]', ...args]);
}

export default class CheckBoxList extends PureComponent {
  static propTypes = {
    className: PropTypes.string,
    handlerExtractor: PropTypes.func.isRequired,
    idExtractor: PropTypes.func.isRequired,
    selected: PropTypes.instanceOf(Set),
    itemClassName: PropTypes.string,
    items: ImmutablePropTypes.iterable.isRequired,
    keyExtractor: PropTypes.func.isRequired,
    labelExtractor: PropTypes.func.isRequired,
  };

  static defaultProps = {
    className: '',
    itemClassName: '',
    selected: Set(),
  };

  render() {
    const {
      items,
      className,
      keyExtractor,
      itemClassName,
      idExtractor,
      handlerExtractor,
      selected,
      labelExtractor,
    } = this.props;
    return (
      <ul className={className}>
        {items.map(item => (
          <li key={keyExtractor(item)} className={itemClassName}>
            <label htmlFor={idExtractor(item)}>
              <input
                type='checkbox'
                id={idExtractor(item)}
                onChange={e => handlerExtractor(item, e.currentTarget.checked)}
                checked={selected.has(idExtractor(item))}
              />
              <span>{labelExtractor(item)}</span>
            </label>
          </li>
        )).toArray()}
      </ul>
    );
  }
}
