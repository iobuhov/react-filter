import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import classNames from 'classnames';
import { List } from 'immutable';
import Draggable from 'react-draggable';
import Dropdown from 'components/Dropdown/Dropdown';
import SearchContainer from 'containers/SearchContainer';
import './Filter.css';

export default class Filter extends PureComponent {
  static propTypes = {
    className: PropTypes.string,
    contexts: ImmutablePropTypes.iterable.isRequired,
    contextsHandlerExtractor: PropTypes.func.isRequired,
    contextsLabel: PropTypes.string.isRequired,
    dimensions: ImmutablePropTypes.iterable.isRequired,
    dimensionsHandlerExtractor: PropTypes.func.isRequired,
    dimensionsLabel: PropTypes.string.isRequired,
    idExtractor: PropTypes.func.isRequired,
    isSelectedContext: PropTypes.func.isRequired,
    isSelectedDimension: PropTypes.func.isRequired,
    isSelectedRecord: PropTypes.func.isRequired,
    keyExtractor: PropTypes.func.isRequired,
    labelExtractor: PropTypes.func.isRequired,
    recordHandlerExtractor: PropTypes.func.isRequired,
    recordTextExtractor: PropTypes.func.isRequired,
    records: ImmutablePropTypes.iterable,
  };

  static defaultProps = {
    className: '',
    records: List(),
  };

  render() {
    const {
      className,
      contexts,
      contextsHandlerExtractor,
      contextsLabel,
      dimensions,
      dimensionsHandlerExtractor,
      dimensionsLabel,
      isSelectedContext,
      isSelectedDimension,
      isSelectedRecord,
      recordHandlerExtractor,
      recordTextExtractor,
      records,
      ...rest
    } = this.props;
    const rootClass = classNames('filter', className);
    return (
      <Draggable
        axis='both'
        handle='.filter__btn-move'
      >
        <div className={rootClass}>
          <div className='filter__header'>
            <div className='filter__btn-move'>
              <span />
              <span />
              <span />
            </div>
            <h3 className='filter__heading'>Filter</h3>
          </div>
          <div className='filter__controls'>
            <Dropdown
              label={contextsLabel}
              items={contexts}
              handlerExtractor={contextsHandlerExtractor}
              selected={contexts.filter(isSelectedContext)}
              {...rest}
            />
            <Dropdown
              label={dimensionsLabel}
              items={dimensions.size ? dimensions : List()}
              disabled={dimensions.size === 0}
              selected={dimensions.filter(isSelectedDimension)}
              handlerExtractor={dimensionsHandlerExtractor}
              {...rest}
            />
          </div>
          <div className='filter__records'>
            <SearchContainer
              items={records}
              textExtractor={item => item.name}
              isSelected={isSelectedRecord}
              handlerExtractor={recordHandlerExtractor}
              {...rest}
            />
          </div>
        </div>
      </Draggable>
    );
  }
}
