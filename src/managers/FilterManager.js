import { List } from 'immutable';
import ContextRecord from 'records/ContextRecord';
import DimensionRecord from 'records/DimensionRecord';
import FilterRecord from 'records/FilterRecord';
import axios from 'axios';

const API_PORT = '3030';
const API_HOST = 'http://0.0.0.0';
const REQUEST_URL = `${API_HOST}:${API_PORT}`;

function getInitData() {
  return axios.get(`${REQUEST_URL}/tables`);
}

function getRecords(tableId, columnId) {
  return axios.get(
    `${REQUEST_URL}/records?tableId=${tableId}&columnId=${columnId}`
  );
}

function parseResponse(response) {
  function reducer(result, entry) {
    const { columns, ...context } = entry;
    const { contexts = [], dimensions = [] } = result;
    return {
      contexts: contexts.concat(
        new ContextRecord(context)
      ),
      dimensions: dimensions.concat(columns.map(
        item => new DimensionRecord({
          id: item.columnId,
          name: item.name,
          tableId: context.id,
        }))),
    };
  }
  const data = response.data;
  const parsed = data.reduce(reducer, {});
  return {
    contexts: List(parsed.contexts),
    dimensions: List(parsed.dimensions),
    records: List(),
    rawData: List(data),
  };
}

function parseRecords(response) {
  const { content, columnId, tableId } = response.data.pop();
  return List(content.map((value, idx) => new FilterRecord({
    id: idx,
    tableId,
    columnId,
    name: value,
  })));
}

export default {
  getInitData,
  getRecords,
  parseRecords,
  parseResponse,
};
