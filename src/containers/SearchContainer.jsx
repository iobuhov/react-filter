import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import cs from 'classnames';
import ReactIScroll from 'react-iscroll';
import iScroll from 'iscroll';
import { Map } from 'immutable';
import FilterStore from 'stores/FilterStore';
import SearchStore from 'stores/SearchStore';
import Search from 'components/Search/Search';
import CheckBoxList from 'components/CheckBoxList/CheckBoxList';


const iScrollOptions = {
  scrollbars: true,
  mouseWheel: true,
  fadeScrollbars: true,
  click: false,
};

export default class SearchContainer extends Component {
  static propTypes = {
    className: PropTypes.string,
    items: ImmutablePropTypes.list.isRequired, // eslint-disable-line
    idExtractor: PropTypes.func.isRequired,
    isSelected: PropTypes.func.isRequired,
    handlerExtractor: PropTypes.func.isRequired,
    keyExtractor: PropTypes.func.isRequired,
    labelExtractor: PropTypes.func.isRequired,
    textExtractor: PropTypes.func.isRequired,
  };

  static defaultProps = {
    className: '',
  };

  state = {
    searchState: SearchStore.getState(),
    filterState: FilterStore.getState(),
  };

  componentDidMount() {
    this.tokens = Map({
      search: SearchStore.addListener(this.onStoreChange),
      filter: FilterStore.addListener(this.onStoreChange),
    });
  }

  componentWillUnmount() {
    this.tokens.forEach(token => token.remove());
  }

  // TODISCUSS: Should each component implement
  // callback method for each store separately?
  onStoreChange = () => this.setState({
    searchState: SearchStore.getState(),
    filterState: FilterStore.getState(),
  });

  getItems = () =>
    this.state.filterState.records.filter(
      r =>
        this.state.filterState.selectedTree
        .hasIn([r.tableId, r.columnId])
    );

  sort = (items) => {
    if (this.state.searchState.sortAlphabetically) {
      return items;
    }
    return items.reverse();
  }

  render() {
    // TODISCUSS: How to transfer props?
    const {
      className,
      idExtractor,
      isSelected,
      keyExtractor,
      labelExtractor,
      textExtractor,
      handlerExtractor,
    } = this.props;
    const rootClass = cs('search-container', className);
    const items = this.getItems().filter(
      record => this.state.searchState.predicate(
        textExtractor(record).toLowerCase(),
        this.state.searchState.searchString.toLowerCase()
      )
    );
    const selected = items.filter(isSelected).map(idExtractor).toSet();
    return (
      <div className={rootClass}>
        <Search />
        <div className='filter__results'>
          <ReactIScroll iScroll={iScroll} options={iScrollOptions}>
            <CheckBoxList
              items={this.sort(items)}
              selected={selected}
              idExtractor={idExtractor}
              keyExtractor={keyExtractor}
              labelExtractor={labelExtractor}
              handlerExtractor={handlerExtractor}
            />
          </ReactIScroll>
        </div>

      </div>
    );
  }
}
