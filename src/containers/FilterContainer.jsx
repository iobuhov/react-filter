import React, { PureComponent } from 'react';
import { Map } from 'immutable';
import * as FilterActions from 'actions/FilterActions';
import FilterStore from 'stores/FilterStore';
import Filter from 'components/Filter/Filter';

const log = (...args) => // eslint-disable-line
      console.log.apply(console, ['[CONTAINER]', ...args]);

const idFormat = (...args) =>
      args.filter(x => !!x).join('-');
const idExtractor = ({ id, tableId, columnId }) =>
      idFormat(tableId, columnId, id);
const labelExtractor =
      item => item.name;
const recordTextExtractor =
      item => item.name;

function contextsHandlerExtractor(item, isChecked) {
  FilterActions.toggleContext(item.id, isChecked);
}

function dimensionHandlerExtractor(item, isChecked) {
  FilterActions.toggleDimension(item.tableId, item.id, isChecked);
  if (isChecked) {
    FilterActions.getRecords(item.tableId, item.id);
  }
}

function recordHandlerExtractor(item, isChecked) {
  FilterActions.toggleRecord(item.tableId, item.columnId, item.id, isChecked);
}

class FilterContainer extends PureComponent {
  componentDidMount() {
    this.token = FilterStore.addListener(this.onStateChange);
    FilterActions.init();
  }

  componentWillUnmount() {
    this.token.remove();
  }

  onStateChange = () =>
    this.setState({ filters: FilterStore.getState() });

  isSelectedContext = context =>
    this.state.filters.selectedTree
        .has(context.id)

  isSelectedDimension = dimension =>
    this.state.filters.selectedTree
        .hasIn([dimension.tableId, dimension.id])

  isSelectedRecord = record =>
    this.state.filters.selectedTree
        .getIn([record.tableId, record.columnId])
        .has(record.id)

  filterDimensions = () => { //eslint-disable-line
    return this.state.filters.dimensions
      .filter(item => this.state.filters.selectedTree.has(item.tableId));
  }

  filterRecords = () => this.state.filters.records
    .filter(item => this.state.filters.selectedTree.get(item.tableId, Map()).has(item.columnId))

  render() {
    const isLoaded = this.state;
    if (isLoaded) {
      const contexts = this.state.filters.contexts;
      const dimensions = this.filterDimensions();
      const records = this.filterRecords();
      return (
        <Filter
          contexts={contexts}
          dimensions={dimensions}
          records={records}
          contextsLabel={'Contexts'}
          dimensionsLabel={'Dimensions'}
          idExtractor={idExtractor}
          keyExtractor={idExtractor}
          labelExtractor={labelExtractor}
          contextsHandlerExtractor={contextsHandlerExtractor}
          dimensionsHandlerExtractor={dimensionHandlerExtractor}
          recordHandlerExtractor={recordHandlerExtractor}
          recordTextExtractor={recordTextExtractor}
          isSelectedContext={this.isSelectedContext}
          isSelectedDimension={this.isSelectedDimension}
          isSelectedRecord={this.isSelectedRecord}
        />
      );
    }
    return <div>Loading...</div>;
  }
}

export default FilterContainer;
