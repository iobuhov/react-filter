import FilterDispatcher from 'dispatchers/FilterDispatcher';
import FilterConstants from 'constants/FilterConstants';
import FilterManager from 'managers/FilterManager';

const dispatch = action =>
      FilterDispatcher.dispatch(action);

const log = (...args) =>
      console.log.apply(console, ['[ACTIONS]', ...args]);

export const getInitDataSuccess = spec =>
  dispatch({ type: FilterConstants.GET_INIT_DATA_SUCCESS, spec });

export const getRecordsSuccess = records =>
  dispatch({ type: FilterConstants.GET_RECORDS_SUCCESS, records });

export const requestFail = (error) => {
  dispatch({ type: FilterConstants.REQUEST_FAIL, error });
  log(error);
};

export const getRecords = (tableId, columnId) => {
  dispatch({ type: FilterConstants.GET_RECORDS });
  FilterManager
    .getRecords(tableId, columnId)
    .then(response => getRecordsSuccess(FilterManager.parseRecords(response)))
    .catch(requestFail);
};

export const init = () => {
  dispatch({ type: FilterConstants.INIT });
  FilterManager
    .getInitData()
    .then(response => getInitDataSuccess(FilterManager.parseResponse(response)))
    .catch(requestFail);
};

export const toggleContext = (id, isChecked) =>
  dispatch({
    type: FilterConstants.TOGGLE_CONTEXT,
    isChecked,
    id,
  });

export const toggleDimension = (tableId, id, isChecked) =>
  dispatch({
    type: FilterConstants.TOGGLE_DIMENSION,
    isChecked,
    tableId,
    id,
  });

export const toggleRecord = (tableId, columnId, id, isChecked) =>
  dispatch({
    type: FilterConstants.TOGGLE_RECORD,
    isChecked,
    tableId,
    columnId,
    id,
  });
