import FilterDispatcher from 'dispatchers/FilterDispatcher';
import SearchConstants from 'constants/SearchConstants';

const dispatch = action =>
      FilterDispatcher.dispatch(action);

const log = (...args) => // eslint-disable-line
      console.log.apply(console, ['[SEARCH ACTIONS]', ...args]);

export const searchStringChange = str =>
  dispatch({ type: SearchConstants.SEARCH_STRING_CHANGE, str });

export const searchTypeChange = id =>
  dispatch({ type: SearchConstants.SEARCH_TYPE_CHANGE, id });

export const searchSortChange = () =>
  dispatch({ type: SearchConstants.SEARCH_SORT_CHANGE });
